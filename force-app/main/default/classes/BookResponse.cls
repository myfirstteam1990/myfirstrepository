public class BookResponse{    
    public Integer totalItems{get;set;}
    public List<Book> Books{get;set;}
    
    public class Book{       
        public string selfLink{get;set;}        
    	public InfoWrap volumeInfo{get;set;}

    }
    
    public class InfoWrap{
        public string title{get;set;}
        public List<string> authors{get;set;}
        public List<idWrap> industryIdentifiers{get;set;}        
    }
    
    public class idWrap{
        public String type{get;set;}
        public String identifier{get;set;}
    }
}