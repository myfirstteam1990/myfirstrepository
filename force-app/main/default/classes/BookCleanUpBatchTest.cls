@isTest
private class BookCleanUpBatchTest {
	@testSetup
    static void genrateTestdata(){        
        List<Reading_List__c> rList = new List<Reading_List__c>();
        List<Book__c> bkList = new List<Book__c>();
        
        for(Integer i=1;i<=3;i++){
            rList.add(new Reading_List__c(name='Realted List '+i));
        }
		insert rList; 
        
        for(Reading_List__c rl: rList){
           for(Integer i=1;i<=3;i++){
            bkList.add(new Book__c(title__c='Book title '+i,author__c='Book Author '+i, ISBN__c=1234567890, Reading_List__c=rl.ID));            
        	} 
        }
        insert bkList;
    }
    
    @isTest
    static void dobookcleanupTest(){
        List<Book__c> delBks=[Select Id from Book__c where title__c like '%3'];
        Test.startTest();
        	BookCleanUpBatch bkc = new BookCleanUpBatch();
        	Id jobId= Database.executeBatch(bkc);
        Test.stopTest();
        system.assertEquals(6, [Select count() from Book__c]);
    }
}