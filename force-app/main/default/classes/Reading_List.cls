public class Reading_List {
    public Reading_List__c rl{get;set;}
    public List<WrapBook> viewBooks{get;set;}
    public List<Book__c> Books{get;set;}
    public List<Book__c> selectedBooks{get;set;}
    public Reading_List (){
        this.Books= [Select title__c, Author__c, ISBN__c, Reading_List__c  from Book__C where Reading_List__C IN 
                        (select Id from Reading_List__c where User__c= :ApexPages.CurrentPage().getParameters().get('Id'))];
    }
    
    public void removeBooks(){
        for(WrapBook x: viewBooks){
            if(x.ckbox == true) {
            x.bk.Reading_List__c = null;
            selectedBooks.add(x.bk);
            }
        }
        update selectedBooks;        
    }
    
    public void MarkRead(){
        for(WrapBook x: viewBooks){
            if(x.ckbox == true) {
            x.bk.Read__c = true;
            selectedBooks.add(x.bk);
            }
        }
        update selectedBooks;        
    }
                 
         
         public class WrapBook{
            public book__c bk{get;set;}
            public boolean ckbox{get;set;}
            public WrapBook(book__c bk){
            this.bk=bk;
            this.ckbox=false;
         }
    }
}