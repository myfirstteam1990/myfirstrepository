public class Bookitems{
        public string selfLink{get;set;}
        public InfoWrap volumeInfo{get;set;}
        
        public class InfoWrap{
        public string title{get;set;}
        public List<String> authors{get;set;}
        public List<IdentifierWrap> industryIdentifiers{get;set;}
        }
        
        public class IdentifierWrap{
            public string type{get;set;}
            public string identifier{get;set;}
        }
}