public class SearchandAddBooks {
    public String searchTerm{get;set;}
    public String searchIn{get;set;}
    public List<BookWrap> bwrap{get;set;}
    public List<Book__c> selectedBooks{get;set;}
    public Book__c bks{get;set;}
    
   GoogleBooks__c  CS = GoogleBooks__c.getOrgDefaults(); 
   public String googleAPIEndPoint = cs.APIURL__C + searchTerm + '+' +searchIn;
 
 //Show as well as Present option to create new new Reading List for Addming Books
    public SearchandAddBooks (){
        if(bks  == null) {bks= new Book__c();}
        this.bks = [Select Reading_List__c from Book__c where Reading_List__r.User__c = :ApexPages.CurrentPage().getParameters().get('id')];
    }

//Search and Display Books based on Title, Author or ISBN   
    public PageReference SearchBooks(){
        //Make HTTP callout
        BookitemsWrap b= new BookitemsWrap();
        Http h = new Http();
        HttpRequest req= new HttpRequest();
        req.setEndPoint(googleAPIEndPoint);
        req.setMethod('GET');
        req.setHeader('content-Type','application/json');
        HttpResponse res = h.send(req);
        //Parse Response
        if(res.getStatusCode() == 200){
           b = (BookitemsWrap)JSON.deserialize(res.getBody(), BookitemsWrap.class); 
        }
        
        for(Bookitems x: b.items){ 
            system.debug('first Book: ' + x);           
           if(bwrap == null) {bwrap = new List<BookWrap>();} 
           bwrap.add(new BookWrap(x));
           
        }
         system.debug(bwrap);
        return null;
    }
    
    //Add Books to a List
    public void AddBooks(){
        if(bwrap != null){
            for(BookWrap bw: bwrap ){
                 if(bw.ckbox == true){
                         selectedBooks.add(new Book__c(title__c=bw.book.volumeInfo.title,author__c=bw.book.volumeInfo.authors[0]
                                                                                 ,ISBN_New__c=bw.book.volumeInfo.industryIdentifiers[0].identifier, Reading_List__c = bks.Reading_List__c ));                
                }
            }           
        } 
        
        insert selectedBooks;
    }
    
    public class BookWrap{
         public Bookitems book{get;set;}
         public boolean ckbox{get;set;}
         
         public BookWrap(Bookitems book){
             this.book=book;
             this.ckbox=false;
         }
    }
        
}