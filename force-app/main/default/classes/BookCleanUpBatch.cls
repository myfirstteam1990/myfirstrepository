public class BookCleanUpBatch implements Database.Batchable<sObject>, Schedulable {
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('elect Id from Book__c where Reading_List__c = NULL');
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Book__C> bks = (List<Book__c>)scope;
        delete bks;
    }
    
    public void finish(Database.BatchableContext BC){
        
       AsyncApexJob jb = [select JobItemsProcessed, NumberOfErrors from AsyncApexJob where Id = :bc.getJobId()];
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new string[]{'prabhat.kumar5990@outlook.com'});
        mail.setSubject('Batach clean for Books done');
        mail.setPlainTextBody('Job Details:' +jb);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail} );
    }
    
    public void  execute(SchedulableContext sc){
        Id jobId=Database.executeBatch(new BookCleanUpBatch());        
    }
}